import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

import java.util.ArrayList;

public class LoginTest {


    @Test
    public void SimpleLoginTest() throws InterruptedException {

        String PATH_CHROMEDRIVER = "C:\\ChromeDriver\\chromedriver.exe";
        String LINK_CV_CREATOR = "https://qa-cvcreator.cognizantsoftvision.com";
        String EMAIL = "miguel.centellas@softvision.com";
        String PASSWORD = "softvision627";

        System.setProperty("webdriver.chrome.driver", PATH_CHROMEDRIVER);
        // WebDriver driver = new ChromeDriver();


        //MIGUEL ENG
        FirefoxOptions options = new FirefoxOptions();
        options.setCapability("--disable-web-security",true);
        options.setCapability("--user-data-dir",true);
        options.setCapability("--allow-running-insecure-content",true);
        //args: ['--disable-web-security','--user-data-dir','allow-running-insecure-content'

        FirefoxProfile profile = new FirefoxProfile();

        options.setProfile(profile);
        // MIGUEL ENG


        System.setProperty("webdriver.gecko.driver", "C:\\sw\\geckodriver.exe");
        WebDriver driver = new FirefoxDriver(options);

/*
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-web-security");
        options.addArguments("--user-data-dir");
        options.addArguments("allow-running-insecure-content");
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        ChromeDriverService service = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File(PATH_CHROMEDRIVER))
                .usingAnyFreePort()
                .build();
        options.merge(cap);
        ChromeDriver driver = new ChromeDriver(service, options);

        /*
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--disable-web-security");
     //   options.addArguments("--user-data-dir");
        options.addArguments("allow-running-insecure-content");
     //   DesiredCapabilities dc = DesiredCapabilities;
       // dc.setCapability(ChromeOptions.CAPABILITY, options);

        WebDriver driver = new ChromeDriver(options);
*/


        driver.get(LINK_CV_CREATOR);

        WebElement signWithGoogle = driver.findElement(By.xpath("//button"));
        signWithGoogle.click();
        Thread.sleep(3000);

        //App opens other window to login
        ArrayList<String> windows = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(windows.get(1));

        driver.findElement(By.xpath("//input[@name='identifier']")).sendKeys(EMAIL);
        driver.findElement(By.xpath("//span[text()='Siguiente']")).click();

        //For now, We add the timewait since of thread for load to next step
        Thread.sleep(3000);

        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(PASSWORD);
        driver.findElement(By.xpath("//span[text()='Siguiente']")).click();

        Thread.sleep(10000);


        driver.quit();
    }

}
